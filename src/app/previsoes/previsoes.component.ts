import { Component, Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import JqueryCSV from 'node_modules/jquery-csv/src/jquery.csv.js'
//import * as $ from "jquery-csv";

// Import do leitor de csv
// https://github.com/typeiii/jquery-csv
//var csv = require('./jquery.csv.js');





@Component({
  selector: 'app-previsoes',
  templateUrl: './previsoes.component.html',
  styleUrls: ['./previsoes.component.css']
})
export class PrevisoesComponent implements OnInit {

   /** Language object from the Json file */
   public txt  = null;

   public mycsv;

  constructor(private http : HttpClient) { }

  

  ngOnInit(): void {

    let filePath :string = 'assets/resultados.csv';

    let aux;

    this.http.get(filePath, { responseType: 'text' })
      .subscribe(
        data => {
          console.log(data);
          aux = data;
          console.log(aux);
          this.txt = aux;
          console.log("Final");
          console.log(this.txt);
          //this.mycsv = JqueryCSV.function(this.txt);
          this.mycsv = JqueryCSV.toObjects(this.txt);
          //this.mycsv = $.csv.toObjects(this.txt);
        },
        err => {
          console.log(err);
        }
      );

    let j = 1; // Permite breakpoint in debug.
    
  }

  /** Teste, debug. */
  public doTest(): void{
    console.log("OK");
    console.log(this.mycsv)
  }

}
