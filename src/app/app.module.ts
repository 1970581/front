import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HistoricoComponent } from './historico/historico.component';
import { PrevisoesComponent } from './previsoes/previsoes.component';
import { FormsModule } from '@angular/forms';

import { HttpClientModule }    from '@angular/common/http';
import { ServidorComponent } from './servidor/servidor.component';

import { ChartModule } from '@syncfusion/ej2-angular-charts';
import {CategoryService, DateTimeService, ScrollBarService, ColumnSeriesService, LineSeriesService, 
  ChartAnnotationService, RangeColumnSeriesService, StackingColumnSeriesService,LegendService, TooltipService, DataLabelService} from '@syncfusion/ej2-angular-charts';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HistoricoComponent,
    PrevisoesComponent,
    ServidorComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartModule
  ],
  providers: [CategoryService, DateTimeService, ScrollBarService, LineSeriesService, ColumnSeriesService, 
    ChartAnnotationService, RangeColumnSeriesService, StackingColumnSeriesService, LegendService, TooltipService, DataLabelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
