import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { HistoricoComponent} from './historico/historico.component'
import { PrevisoesComponent} from './previsoes/previsoes.component'
import { ServidorComponent} from './servidor/servidor.component'


const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'historico', component: HistoricoComponent },
  { path: 'previsoes', component: PrevisoesComponent },
  { path: 'servidor', component: ServidorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
