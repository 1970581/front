import { Component, Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import JqueryCSV from 'node_modules/jquery-csv/src/jquery.csv.js'
import { Equipas } from 'src/app/servicos/equipas'
import { isNgTemplate } from '@angular/compiler';

//import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-historico',
  templateUrl: './historico.component.html',
  styleUrls: ['./historico.component.css']
})
export class HistoricoComponent implements OnInit {

  /** String read from file **/
  public txt  = null;
  /** Object with the array of the historical data from csv */
  public mycsv;
  /** Array of data to show on table and be modified */
  public dataToShow;
  /** All team names */
  public Teams = Equipas.getTeams();
  /** The team selected on combo box */
  public selectedTeam;    

  constructor(private http : HttpClient) { }

  ngOnInit(): void {
    
    let filePath :string = 'assets/dataset.csv';    

    this.http.get(filePath, {responseType: 'text'})
        .subscribe(
          data => {
            //console.log(data);                   //debug
            this.txt = data;
            this.mycsv = JqueryCSV.toObjects(this.txt);  //Original backup
            this.dataToShow = JqueryCSV.toObjects(this.txt); // Usado para mostrar e ser alterado 
            
            // mostrar os mais recentes.
            this.mycsv.reverse();
            this.dataToShow.reverse();           
          },
          err => {
            console.log(err);
          });
  }

  public doTest(){
    console.log(this.Teams);
    //console.log(Equipas.todasEquipas);
  }

  public clearSelection(){
    console.log(this.selectedTeam);
    this.selectedTeam = "";
    this.dataToShow = this.mycsv.slice();
  }

  public updateTableBasedOnSelection(){
    this.dataToShow = [];
    console.log("Selected option is: " + this.selectedTeam);
    this.mycsv.forEach(entry => {
      if (entry.HomeTeam == this.selectedTeam || entry.AwayTeam == this.selectedTeam) this.dataToShow.push(entry);
    } );
  }

  

  /**
   * Transforma um valor negativo em positivo, para ajudar a tabela HTML
   * @param value 
   */
  public negative_to_positive(value){
    return -1 * value;
  }

}
