export class Equipas {

public static todasEquipas : string[] = ['Arsenal', 'Leicester', 'Brighton', 'Man City', 'Chelsea', 'Burnley',
'Crystal Palace', 'Huddersfield', 'Everton', 'Stoke', 'Southampton', 'Swansea',
'Watford', 'Liverpool', 'West Brom', 'Bournemouth', 'Man United', 'West Ham',
'Newcastle', 'Tottenham', 'Cardiff', 'Fulham', 'Wolves', 'Norwich',
'Sheffield United', 'Aston Villa', 'Leeds']

public static getTeams(){
    return Equipas.todasEquipas;
}

}