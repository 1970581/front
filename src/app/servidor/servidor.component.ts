import { Component, OnInit } from '@angular/core';
import { Equipas } from 'src/app/servicos/equipas'
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import JqueryCSV from 'node_modules/jquery-csv/src/jquery.csv.js'
import { isNgTemplate } from '@angular/compiler';
import {ChartModule,LineSeriesService} from '@syncfusion/ej2-angular-charts';

@Component({
  selector: 'app-servidor',
  templateUrl: './servidor.component.html',
  styleUrls: ['./servidor.component.css']
})

/** Classe de previsões do servidor que gradualmente foi expandida para mostrar estatisticas e que deveria ser refactorada em mais classes */
export class ServidorComponent implements OnInit {

  /** All team names */
  public Teams = Equipas.getTeams();
  /** The team selected on combo box */
  public homeTeam : String;
  public awayTeam : String;
  /** Resultado devolvido pelo servidor */
  public resultado_servidor = "";
  public equipas_vs = "";

  /** String read from file **/
  public txt = null;
  /** Object with the array of the historical data from csv */
  public mycsv;
  /** Array of data to show on table and be modified */
  public dataToShow;
  /** If we should show the table */
  public showTable : boolean = false;//false;

  /** ESTATISTICAS */
  public statmareHome :number = 0;
  public statmareAway :number = 0;
  public statjogosHome : number =0;
  public statjogosAway : number =0;
  public statgolosHome : number = 0;
  public statgolosAway : number = 0;
  public statvitoriasHome :number = 0;
  public statvitoriasAway :number = 0;
  public statempatesHome :number = 0;
  public statempatesAway :number = 0;
  public statderrotasHome :number = 0;
  public statderrotasAway :number = 0;
  public statgolos0Home :number = 0;
  public statgolos1Home :number = 0;
  public statgolos2Home :number = 0;
  public statgolos3Home :number = 0;
  public statgolos0Away :number = 0;
  public statgolos1Away :number = 0;
  public statgolos2Away :number = 0;
  public statgolos3Away :number = 0;

  /** Estatisticas por Visitante e por Casa */
  public statjogosHomeH : number =0;
  public statjogosAwayH : number =0;
  public statgolosHomeH : number = 0;
  public statgolosAwayH : number = 0;
  public statvitoriasHomeH :number = 0;
  public statvitoriasAwayH :number = 0;
  public statempatesHomeH :number = 0;
  public statempatesAwayH :number = 0;
  public statderrotasHomeH :number = 0;
  public statderrotasAwayH :number = 0;
  public statgolos0HomeH :number = 0;
  public statgolos1HomeH :number = 0;
  public statgolos2HomeH :number = 0;
  public statgolos3HomeH :number = 0;
  public statgolos0AwayH :number = 0;
  public statgolos1AwayH :number = 0;
  public statgolos2AwayH :number = 0;
  public statgolos3AwayH :number = 0;

  public statjogosHomeA : number =0;
  public statjogosAwayA : number =0;
  public statgolosHomeA : number = 0;
  public statgolosAwayA : number = 0;
  public statvitoriasHomeA :number = 0;
  public statvitoriasAwayA :number = 0;
  public statempatesHomeA :number = 0;
  public statempatesAwayA :number = 0;
  public statderrotasHomeA :number = 0;
  public statderrotasAwayA :number = 0;
  public statgolos0HomeA :number = 0;
  public statgolos1HomeA :number = 0;
  public statgolos2HomeA :number = 0;
  public statgolos3HomeA :number = 0;
  public statgolos0AwayA :number = 0;
  public statgolos1AwayA :number = 0;
  public statgolos2AwayA :number = 0;
  public statgolos3AwayA :number = 0;


  /** Objectos para construcao do grafico g1 e g2 */
  public g1_chartData : Object[];
  public g1_primaryXAxis: Object;
  public g1_primaryYAxis: Object;
  public g1_title : string;
  public g1_columnMarker : Object;
  public g1_tooltip : Object;

  public g2_chartData : Object[];
  public g2_primaryXAxis: Object;
  public g2_primaryYAxis: Object;
  public g2_title : string;
  public g2_columnMarker : Object;
  public g2_tooltip : Object;

  public g3_chartData : Object[];
  public g3_primaryXAxis: Object;
  public g3_primaryYAxis: Object;
  public g3_title : string;
  public g3_columnMarker : Object;
  public g3_tooltip : Object;

  public g4_chartData : Object[];
  public g4_primaryXAxis: Object;
  public g4_primaryYAxis: Object;
  public g4_title : string;
  public g4_columnMarker : Object;
  public g4_tooltip : Object;
  

  constructor(private http: HttpClient) { }

  ngOnInit(): void {





    let filePath: string = 'assets/dataset.csv';

    this.http.get(filePath, { responseType: 'text' })
      .subscribe(
        data => {
          //console.log(data);                   //debug
          this.txt = data;
          this.mycsv = JqueryCSV.toObjects(this.txt);  //Original backup
          this.dataToShow = JqueryCSV.toObjects(this.txt); // Usado para mostrar e ser alterado 

          // mostrar os mais recentes.
          this.mycsv.reverse();
          this.dataToShow.reverse();
        },
        err => {
          console.log(err);
        });
        //this.do_chart();  // DEBUG
  }

  /**
   * Efectua o pedido HTML ao servidor e faz update da tabela.
   */
  public pressButton(){
    console.log(this.homeTeam + " " + this.awayTeam);

    // Validar que temos 2 equipas
    if( this.homeTeam == null || this.homeTeam == undefined || this.homeTeam == "" ||
        this.awayTeam == null || this.awayTeam == undefined || this.awayTeam == ""
    ){
      alert("Por favor selecionar duas equipas.");
      return;
    }
    if( this.homeTeam == this.awayTeam){
      alert("Por favor selecionar duas equipas diferentes.");
      return;
    }


    //Colocar o texto.
    this.equipas_vs = this.homeTeam + " VS " + this.awayTeam;
    this.resultado_servidor = "";

    // Mostrar a tabela.
    this.filtrar_jogos_das_duas_equipas();
    this.showTable = true;

    //Variaveis de ambiente estao nos ficheiros enviroment.ts e environment.prod.ts, contem o URL

    var myUrl :string = environment.server_url + "?home=" + this.homeTeam + "&away=" + this.awayTeam;

    this.http.get(myUrl, {responseType: 'text'})
        .subscribe(
          data => {
            //console.log(data);                   //debug
            this.resultado_servidor = data;                 
          },
          err => {
            console.log(err);
          });
  }

  /** Transforma um valor negativo em positivo, para ajudar a tabela HTML */
  public negative_to_positive(value){
    return -1 * value;
  }

  /** Soma dois valores */
  public somar_valores(value1, value2){
    return parseInt(value1) + parseInt(value2);
  }

  public percentagem(valor, total){
    let a : number = parseInt(valor);
    let b : number = parseInt(total);
    if (b <= 9) return 0;
    let resultado = (a/b)*100;
    return resultado.toFixed(0);
  }

  /** Filtra a tabela para aparecerem apenas os jogos das duas equipas */
  public filtrar_jogos_das_duas_equipas(){

    // Validar que temos 2 equipas
    if( this.homeTeam == null || this.homeTeam == undefined || this.homeTeam == "" ||
        this.awayTeam == null || this.awayTeam == undefined || this.awayTeam == ""
    ){
      return;
    }
    if( this.homeTeam == this.awayTeam)  return;
        
    this.dataToShow = [];    


    // Select de apenas as 2 equipas
    this.mycsv.forEach(entry => {
      if ((entry.HomeTeam == this.homeTeam || entry.AwayTeam == this.homeTeam) &&
        (entry.HomeTeam == this.awayTeam || entry.AwayTeam == this.awayTeam)) {
        this.dataToShow.push(entry);
      }
    });
    this.showTable = true;

    //Testes:
    // Leeds VS Stoke não tem jogos

    // Vamos calcular as estatisitcas.
    this.calcular_estatisticas();
    this.do_chart();
  }


  public calcular_estatisticas(){
    let mareHome :number = 0;
    let mareAway :number = 0;
    let jogosHome : number =0;
    let jogosAway : number =0;
    let golosHome : number = 0;
    let golosAway : number = 0;
    let vitoriasHome :number = 0;
    let vitoriasAway :number = 0;
    let empatesHome :number = 0;
    let empatesAway :number = 0;
    let derrotasHome :number = 0;
    let derrotasAway :number = 0;
    let golos0Home :number = 0;
    let golos1Home :number = 0;
    let golos2Home :number = 0;
    let golos3Home :number = 0;
    let golos0Away :number = 0;
    let golos1Away :number = 0;
    let golos2Away :number = 0;
    let golos3Away :number = 0;

    let HjogosHome : number =0;
    let HjogosAway : number =0;
    let HgolosHome : number = 0;
    let HgolosAway : number = 0;
    let HvitoriasHome :number = 0;
    let HvitoriasAway :number = 0;
    let HempatesHome :number = 0;
    let HempatesAway :number = 0;
    let HderrotasHome :number = 0;
    let HderrotasAway :number = 0;
    let Hgolos0Home :number = 0;
    let Hgolos1Home :number = 0;
    let Hgolos2Home :number = 0;
    let Hgolos3Home :number = 0;
    let Hgolos0Away :number = 0;
    let Hgolos1Away :number = 0;
    let Hgolos2Away :number = 0;
    let Hgolos3Away :number = 0;

    
    let AjogosHome : number =0;
    let AjogosAway : number =0;
    let AgolosHome : number = 0;
    let AgolosAway : number = 0;
    let AvitoriasHome :number = 0;
    let AvitoriasAway :number = 0;
    let AempatesHome :number = 0;
    let AempatesAway :number = 0;
    let AderrotasHome :number = 0;
    let AderrotasAway :number = 0;
    let Agolos0Home :number = 0;
    let Agolos1Home :number = 0;
    let Agolos2Home :number = 0;
    let Agolos3Home :number = 0;
    let Agolos0Away :number = 0;
    let Agolos1Away :number = 0;
    let Agolos2Away :number = 0;
    let Agolos3Away :number = 0;


    this.mycsv.forEach(entry => {

      if (entry.HomeTeam == this.homeTeam){
        jogosHome++;
        HjogosHome++;
        golosHome += parseInt(entry.FTHG);
        if (entry.FTR == 'H') {
          vitoriasHome++;
          HvitoriasHome++;
        }
        if (entry.FTR == 'A') {
          derrotasHome++;
          HderrotasHome++;
        }
        if (entry.FTR == 'D') {
          empatesHome++;
          HempatesHome++;
        }
      }
      if (entry.AwayTeam == this.homeTeam){
        jogosHome++;
        AjogosHome++;
        golosHome += parseInt(entry.FTAG);
        if (entry.FTR == 'A') {
          vitoriasHome++;
          AvitoriasHome++;
        }
        if (entry.FTR == 'H') {
          derrotasHome++;
          AderrotasHome++;
        }
        if (entry.FTR == 'D') {
          empatesHome++;
          AempatesHome++;
        }
      }
      if (entry.HomeTeam == this.awayTeam){
        jogosAway++;
        HjogosAway++;
        golosAway += parseInt(entry.FTHG);
        if (entry.FTR == 'H') {
          vitoriasAway++;
          HvitoriasAway++;
        }
        if (entry.FTR == 'A') {
          derrotasAway++;
          HderrotasAway++;
        }
        if (entry.FTR == 'D') {
          empatesAway++;
          HempatesAway++;
        }
      }
      if (entry.AwayTeam == this.awayTeam){
        jogosAway++;
        AjogosAway++;
        golosAway += parseInt(entry.FTAG);
        if (entry.FTR == 'A') {
          vitoriasAway++;
          AvitoriasAway++;
        }
        if (entry.FTR == 'H') {
          derrotasAway++;
          AderrotasAway++;
        }
        if (entry.FTR == 'D') {
          empatesAway++;
          AempatesAway++;
        }
      }


    });

    // Calculo da Mare homeTeam
    let ignoreHome = false;
    this.mycsv.forEach(entry => {

      if (entry.HomeTeam == this.homeTeam && !ignoreHome){
        mareHome = entry.HomeMare;
        if (entry.FTR == 'H') {
          if( mareHome < 0 ) mareHome = 1;
          else mareHome++;
        }
        if (entry.FTR == 'A') {
          if( mareHome > 0 ) mareHome = -1;
          else mareHome--;
        }
        if (entry.FTR == 'D') mareHome = 0;
        ignoreHome = true;
      }
      if (entry.AwayTeam == this.homeTeam && !ignoreHome){
        mareHome = entry.AwayMare;
        if (entry.FTR == 'A') {
          if( mareHome < 0 ) mareHome = 1;
          else mareHome++;
        }
        if (entry.FTR == 'H') {
          if( mareHome > 0 ) mareHome = -1;
          else mareHome--;
        }
        if (entry.FTR == 'D') mareHome = 0;
        ignoreHome = true;
      }
    });

    // Calculo da mare AwayTeam
    let ignoreAway = false;
    this.mycsv.forEach(entry => {

      if (entry.HomeTeam == this.awayTeam && !ignoreAway){
        mareAway = entry.AwayMare;
        if (entry.FTR == 'H') {
          if( mareAway < 0 ) mareAway = 1;
          else mareAway++;
        }
        if (entry.FTR == 'A') {
          if( mareAway > 0 ) mareAway = -1;
          else mareAway--;
        }
        if (entry.FTR == 'D') mareAway = 0;
        ignoreAway = true;
      }
      if (entry.AwayTeam == this.awayTeam && !ignoreAway){
        mareAway = entry.AwayMare;
        if (entry.FTR == 'A') {
          if( mareAway < 0 ) mareAway = 1;
          else mareAway++;
        }
        if (entry.FTR == 'H') {
          if( mareAway > 0 ) mareAway = -1;
          else mareAway--;
        }
        if (entry.FTR == 'D') mareAway = 0;
        ignoreAway = true;
      }
    });

    // Calculo da distribuicao de golos.
    let golos: number = 0;
    this.mycsv.forEach(entry => {

      if (entry.HomeTeam == this.homeTeam){
        golos = parseInt(entry.FTHG);
        if(golos == 0) golos0Home++;
        if(golos == 1) golos1Home++;
        if(golos == 2) golos2Home++;
        if(golos > 2) golos3Home++;
      }
      if (entry.AwayTeam == this.homeTeam){
        golos = parseInt(entry.FTAG);
        if(golos == 0) golos0Home++;
        if(golos == 1) golos1Home++;
        if(golos == 2) golos2Home++;
        if(golos > 2) golos3Home++;
      }
      if (entry.HomeTeam == this.awayTeam){
        golos = parseInt(entry.FTHG);
        if(golos == 0) golos0Away++;
        if(golos == 1) golos1Away++;
        if(golos == 2) golos2Away++;
        if(golos > 2) golos3Away++;
      }
      if (entry.AwayTeam == this.awayTeam){
        golos = parseInt(entry.FTAG);
        if(golos == 0) golos0Away++;
        if(golos == 1) golos1Away++;
        if(golos == 2) golos2Away++;
        if(golos > 2) golos3Away++;
      }      
    });

    // Calculo da distribuicao de golos. Por visitante e Casa
    golos = 0;
    this.mycsv.forEach(entry => {

      if (entry.HomeTeam == this.homeTeam){
        golos = parseInt(entry.FTHG);
        if(golos == 0) Hgolos0Home++;
        if(golos == 1) Hgolos1Home++;
        if(golos == 2) Hgolos2Home++;
        if(golos > 2) Hgolos3Home++;
      }
      if (entry.AwayTeam == this.homeTeam){
        golos = parseInt(entry.FTAG);
        if(golos == 0) Agolos0Home++;
        if(golos == 1) Agolos1Home++;
        if(golos == 2) Agolos2Home++;
        if(golos > 2) Agolos3Home++;
      }
      if (entry.HomeTeam == this.awayTeam){
        golos = parseInt(entry.FTHG);
        if(golos == 0) Hgolos0Away++;
        if(golos == 1) Hgolos1Away++;
        if(golos == 2) Hgolos2Away++;
        if(golos > 2) Hgolos3Away++;
      }
      if (entry.AwayTeam == this.awayTeam){
        golos = parseInt(entry.FTAG);
        if(golos == 0) Agolos0Away++;
        if(golos == 1) Agolos1Away++;
        if(golos == 2) Agolos2Away++;
        if(golos > 2) Agolos3Away++;
      }      
    });



    this.statmareHome = mareHome;
    this.statmareAway = mareAway;
    this.statjogosHome = jogosHome;
    this.statjogosAway = jogosAway;
    this.statgolosHome = golosHome;
    this.statgolosAway = golosAway;
    this.statvitoriasHome = vitoriasHome;
    this.statvitoriasAway = vitoriasAway;
    this.statempatesHome = empatesHome;
    this.statempatesAway = empatesAway;
    this.statderrotasHome = derrotasHome;
    this.statderrotasAway = derrotasAway;

    this.statgolos0Home = golos0Home;
    this.statgolos1Home = golos1Home;
    this.statgolos2Home = golos2Home;
    this.statgolos3Home = golos3Home;
    this.statgolos0Away = golos0Away;
    this.statgolos1Away = golos1Away;
    this.statgolos2Away = golos2Away;
    this.statgolos3Away = golos3Away;


    this.statjogosHomeH = HjogosHome;
    this.statjogosAwayH = HjogosAway;
    this.statgolosHomeH = HgolosHome;
    this.statgolosAwayH = HgolosAway;
    this.statvitoriasHomeH = HvitoriasHome;
    this.statvitoriasAwayH = HvitoriasAway;
    this.statempatesHomeH = HempatesHome;
    this.statempatesAwayH = HempatesAway;
    this.statderrotasHomeH = HderrotasHome;
    this.statderrotasAwayH = HderrotasAway;

    this.statgolos0HomeH = Hgolos0Home;
    this.statgolos1HomeH = Hgolos1Home;
    this.statgolos2HomeH = Hgolos2Home;
    this.statgolos3HomeH = Hgolos3Home;
    this.statgolos0AwayH = Hgolos0Away;
    this.statgolos1AwayH = Hgolos1Away;
    this.statgolos2AwayH = Hgolos2Away;
    this.statgolos3AwayH = Hgolos3Away;

    this.statjogosHomeA = AjogosHome;
    this.statjogosAwayA = AjogosAway;
    this.statgolosHomeA = AgolosHome;
    this.statgolosAwayA = AgolosAway;
    this.statvitoriasHomeA = AvitoriasHome;
    this.statvitoriasAwayA = AvitoriasAway;
    this.statempatesHomeA = AempatesHome;
    this.statempatesAwayA = AempatesAway;
    this.statderrotasHomeA = AderrotasHome;
    this.statderrotasAwayA = AderrotasAway;

    this.statgolos0HomeA = Agolos0Home;
    this.statgolos1HomeA = Agolos1Home;
    this.statgolos2HomeA = Agolos2Home;
    this.statgolos3HomeA = Agolos3Home;
    this.statgolos0AwayA = Agolos0Away;
    this.statgolos1AwayA = Agolos1Away;
    this.statgolos2AwayA = Agolos2Away;
    this.statgolos3AwayA = Agolos3Away;    


  }

  /** Cria os dados que alimentam os graficos  */
  public do_chart(){
    /*
    this.chartData = [
      { month: 'Jan', sales: 135 }, { month: 'Feb', sales: 28 },
      { month: 'Mar', sales: 34 }, { month: 'Apr', sales: 32 },
      { month: 'May', sales: 40 }, { month: 'Jun', sales: 32 },
      { month: 'Jul', sales: 35 }, { month: 'Aug', sales: 55 },
      { month: 'Sep', sales: 38 }, { month: 'Oct', sales: 30 },
      { month: 'Nov', sales: 25 }, { month: 'Dec', sales: 32 }
    ];

    this.primaryXAxis = {
      valueType: 'Category',
      title: 'my X'
    };
      // Exemplo
    this.g1_chartData = [
      { x: 'A', y1: 10, y2: 20 }, { x: 'B', y1: 28, y2:15 }      
    ];
*/

    // GRAFICO I - golos distribuicao/frequencia
    this.g1_chartData = [
      {x: "0 golos", yHome: this.statgolos0Home, yAway: this.statgolos0Away},
      {x: "1 golos", yHome: this.statgolos1Home, yAway: this.statgolos1Away},
      {x: "2 golos", yHome: this.statgolos2Home, yAway: this.statgolos2Away},
      {x: "3+ golos", yHome: this.statgolos3Home, yAway: this.statgolos3Away}
    ];

    this.g1_primaryXAxis = { valueType: 'Category', title: 'Golos' };
    this.g1_primaryYAxis = { valueType: 'Double',   title: 'Frequência' };
    this.g1_title = "Frequência do número de golos marcados em cada jogo por equipa";
    this.g1_columnMarker = { dataLabel : { visible :true, position: 'Top'}};
    this.g1_tooltip = {enable : true};

    // GRAFICO I -  resultados
    this.g2_chartData = [
      {x: "Vitórias", yHome: this.statvitoriasHome , yAway: this.statvitoriasAway},
      {x: "Empates", yHome: this.statempatesHome, yAway: this.statempatesAway},
      {x: "Derrotas", yHome: this.statderrotasHome, yAway: this.statderrotasAway}      
    ];
    
    this.g2_primaryXAxis = { valueType: 'Category', title: 'Resultado' };
    this.g2_primaryYAxis = { valueType: 'Double',   title: 'Frequência' };
    this.g2_title = "Frequência de vitórias, derrotas e empates por equipa";
    this.g2_columnMarker = { dataLabel : { visible :true, position: 'Top'}};
    this.g2_tooltip = {enable : true};
    
    // Grafico 3 resultados por casa/fora
    this.g3_chartData = [
      {x: "Vitórias", yHomeH: this.statvitoriasHomeH , yHomeA: this.statvitoriasHomeA, yAwayH: this.statvitoriasAwayH, yAwayA: this.statvitoriasAwayA},
      {x: "Empates", yHomeH: this.statempatesHomeH, yHomeA: this.statempatesHomeA, yAwayH: this.statempatesAwayH, yAwayA: this.statempatesAwayA},
      {x: "Derrotas", yHomeH: this.statderrotasHomeH, yHomeA: this.statderrotasHomeA, yAwayH: this.statderrotasAwayH, yAwayA: this.statderrotasAwayA}      
    ];
    
    this.g3_primaryXAxis = { valueType: 'Category', title: 'Resultado' };
    this.g3_primaryYAxis = { valueType: 'Double',   title: 'Frequência' };
    this.g3_title = "Frequência de vitórias, derrotas e empates por equipa em casa e fora";
    this.g3_columnMarker = { dataLabel : { visible :true, position: 'Top'}};
    this.g3_tooltip = {enable : true};
    
    // Grafico 4 golos por casa/fora
    this.g4_chartData = [
      {x: "0 golos", yHomeH: this.statgolos0HomeH, yHomeA: this.statgolos0HomeA, yAwayH: this.statgolos0AwayH, yAwayA: this.statgolos0AwayA},
      {x: "1 golos", yHomeH: this.statgolos1HomeH, yHomeA: this.statgolos1HomeA, yAwayH: this.statgolos1AwayH, yAwayA: this.statgolos1AwayA},
      {x: "2 golos", yHomeH: this.statgolos2HomeH, yHomeA: this.statgolos2HomeA, yAwayH: this.statgolos2AwayH, yAwayA: this.statgolos2AwayA},
      {x: "3+ golos", yHomeH: this.statgolos3HomeH, yHomeA: this.statgolos3HomeA, yAwayH: this.statgolos3AwayH, yAwayA: this.statgolos3AwayA}
    ];
    
    this.g4_primaryXAxis = { valueType: 'Category', title: 'Resultado' };
    this.g4_primaryYAxis = { valueType: 'Double',   title: 'Frequência' };
    this.g4_title = "Frequência de golos por equipa em casa e fora";
    this.g4_columnMarker = { dataLabel : { visible :true, position: 'Top'}};
    this.g4_tooltip = {enable : true};

  }
}
